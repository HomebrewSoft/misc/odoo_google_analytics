{
    "name": "google_analytics",
    "version": "13.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "LGPL-3",
    "depends": [
        "website",
    ],
    "data": [
        # security
        # data
        # reports
        # views
    ],
    "qweb": ["static/src/xml/website.backend.xml"],
}
